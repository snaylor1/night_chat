<?php

/* O L D    P R O G R A M -->

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Load sample data, an array of associative arrays. */
require "models/pms.php";

// Display search form
Route::get('/', function()
{
	return View::make('pms.query');
});

// Perform search and display results
Route::get('search', function()
{
  $query = Input::get('query');

  $results = search($query);

	return View::make('pms.results')->withLibs($results)->with('query', $query);
});


/* Functions for PM database example. */

/* Search sample data for $name or $year or $state from form. */
function search($query) {
  $libs = getLibs();

  // Filter $pms by $name
  if (!empty($query)) 
  {
    $results = array();
    foreach ($libs as $lib) 
    {
      if (stripos($lib['name'], $query) !== FALSE || stripos($lib['address'], $query) !== FALSE || stripos($lib['phoneNum'], $query) !== FALSE || stripos($lib['email'], $query) !== FALSE) 
      {
        $results[] = $lib;
      }
    }
    $libs = $results;
  }
  return $libs;
}
/*
  // Filter $pms by $year
  if (!empty($address)) {
    $results = array();
    foreach ($libs as $lib) {
      if (strpos($lib['address'], $address) !== FALSE) {
        $results[] = $lib;
      }
    }
    $libs = $results;
  }

  // Filter $pms by $state
  if (!empty($phoneNum)) {
    $results = array();
    foreach ($libs as $lib) {
      if (strpos($lib['phoneNum'], $phoneNum) !== FALSE) {
        $results[] = $lib;
      }
    }
    $libs = $results;
  }
  
  // Filter $pms by $year
  if (!empty($email)) {
    $results = array();
    foreach ($libs as $lib) {
      if (strpos($lib['email'], $email) !== FALSE) {
        $results[] = $lib;
      }
    }
    $libs = $results;
  }

  return $libs;
  */
