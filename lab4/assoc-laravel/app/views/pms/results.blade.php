@extends('layouts.master')

@section('title')
Associative array search results page
@stop

@section('content')

<h2>Australian Librarians</h2>
<h3>Results for {{{ $query }}}</h3>

@if (count($libs) == 0)

<p>No results found.</p>

@else 

<table class="bordered">
<thead>
<tr><th>Name</th><th>Address</th><th>Phone Number</th><th>Email</th></tr>
</thead>
<tbody>

@foreach($libs as $lib)
  <tr><td>{{{ $lib['name'] }}}</td><td>{{{ $lib['address'] }}}</td><td>{{{ $lib['phoneNum'] }}}</td><td>{{{ $lib['email'] }}}</td></tr>
@endforeach

</tbody>
</table>
@endif

<form method="get" action="search">
  <table>
    <tr><td>Query: </td><td><input type="text" name="query"></td></tr>
    <tr><td colspan=2><input type="submit" value="Search">
                      <input type="reset" value="Reset"></td></tr>
  </table>
  </form>
@stop

<!-- validated at 2:08 on 27/03/2015 -->