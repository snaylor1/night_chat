drop table if exists posts;

create table posts (
    id integer not null primary key autoincrement,
    username varchar(80) not null,
    title varchar(80) not null,
    message text default ''
); 

insert into posts values (null, "mic bitchum",  "give me attention", "isnert rant here");
insert into posts values (null, "tony jaa", "teach me martial arts or I'll kill myself", "I said it to my dad");
insert into posts values (null, "mike",  "No one, no where", "This was on his something");

drop table if exists comments;

create table comments (
id integer not null primary key autoincrement,
comment_post_id integer not null,
comment_user varchar(80) not null,
comment text default '',
foreign key ( comment_post_id ) references posts ( id )
);