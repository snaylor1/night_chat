
<!DOCTYPE html>

<html>
  <head>
    <title>@yield('title')</title>
  
    <!-- load bootstrap from a cdn -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
  
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

  </head>
  <body>
    @section('content')
      Default content
    @show
  </body>
</html>