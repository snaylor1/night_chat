@extends('layouts.master')

@section('title')
Edit Comment
@stop

@section('content')
<h1>Time Waster</h1>
<h2>Be social.</h2>

<form method="post" action="{{{ url('update_comment_action') }}}">
  <input type="hidden" name="id" value="{{{ $post->id }}}">
  <table>
    <tr><td>Username:</td> <td><input type="text" name="username" value="{{{ $post->username }}}"></td></tr>
    <tr><td>Title:</td> <td><textarea name="title" value="{{{ $post->title }}}"></textarea></td></tr>
    <tr><td>Message:</td> <td><textarea name="message" value="{{{ $post->message }}}"></textarea></td></tr>
    <tr><td colspan=2><input type="submit" value="Save Post"></td></tr>
  </table>
</form>
<a href="{{{ url("home") }}}">Cancel</a>
@stop