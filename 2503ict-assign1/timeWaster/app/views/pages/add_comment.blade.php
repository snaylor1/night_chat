@extends('layouts.master')

@section('title')
Add Comment
@stop

@section('content')
<h1>Time Waster</h1>
<h2>Be social.</h2>

<form method="post" action="add_comment_action">
    <table>
    <tr><td>Username:</td> <td><input type="text" name="username"></td></tr>
    <tr><td>Title:</td> <td><textarea name="title"></textarea></td></tr>
    <tr><td>Message:</td> <td><textarea name="message"></textarea></td></tr>
    <tr><td colspan=2><input type="submit" value="Post!"></td></tr>
    </table>
</form>
<a href="{{{ url("home") }}}">Cancel</a>
@stop