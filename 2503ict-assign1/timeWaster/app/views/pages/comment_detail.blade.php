@extends('layouts.master')

@section('title')
Comment Detail
@stop

@section('content')
<h1>Time Waster</h1>
<h2>Be social.</h2>

<h2>{{{ $post->username }}}</h2>
<h3>{{{ $post->title }}}</h3>
<h4>{{{ $post->message }}}</h4>

<p>
<a href="{{{ url("delete_comment/$post->id") }}}">Delete this Post...</a>
</br>
</br>
<a href="{{{ url("update_comment/$post->id") }}}">Edit this Post...</a>
</br>
</br>
<a href="{{{ url("all_comments/$post->id") }}}">Comment on this Post...</a>
</br>
</br>
<a href="{{{ url("home") }}}">Home</a>
</p>
@stop