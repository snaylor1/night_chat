@extends('layouts.master')

@section('title')
All Comments
@stop

@section('content')
<h1>Time Waster</h1>
<h2>Be social.</h2>
    
<h2>{{{ $post->username }}}</h2>
<h3>{{{ $post->title }}}</h3>
<h4>{{{ $post->message }}}</h4>

<form method="post" action="add_comment_action">
    <input type="hidden" name="id" value="{{{ $post->id }}}">
    <table>
    <tr><td>Commeting User:</td> <td><input type="text" name="comment_user"></td></tr>
    <tr><td>Comment:</td> <td><textarea name="comment"></textarea></td></tr>
    <tr><td colspan=2><input type="submit" value="Post!"></td></tr>
    </table>
</form>
<a href="{{{ url("home") }}}">Cancel</a>
@stop