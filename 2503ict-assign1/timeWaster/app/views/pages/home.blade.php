@extends('layouts.master')

@section('title')
Time Waster - Be social.

@stop

@section('content')
<h1>Time Waster</h1>
<h2>Be social.</h2>

<div class="container">
                    <div class = "row">
                        <div class="col-md-3" id="left">
                            <h3>Posts:</h3><br><br>
                        </div>
                        <div class="col-md-6">
                            <br>
                        @if ($posts)
                        <ul>
                        @foreach(array_reverse($posts) as $post)
                        <li>{{ HTML::image('laravelicon.png') }}<a href="{{{ url("comment_detail/$post->id") }}}">{{{ $post->username }}}, {{{ $post->title }}}, {{{$post->message}}}</a></li>
                        @endforeach
                        </ul>
                        @else
                        <p>No posts found.</p>
                        @endif

                        </div>
                        <div class="col-md-3" id="right">
                            {{-- Right --}}<br><br>
                            <form method="post" action="add_comment_action">
                                <table>
                                    Username:<input type="text" name="username" value=""></br>
                                    Title:<input type="text" name="title" value="">
                                    <tr><td><textarea rows="4" cols="50" name="message"></textarea></td></tr>
                                    <tr><td colspan=2><input type="submit" value="Post!"></td></tr>
                                </table>
                            </form>
                            </br>
                            <tr><td><a href="{{{ url("add_comment") }}}">Add TOTALLY new Post</a></td></tr>
                        </div>
                    </div>
                </div><!-- /.container -->
            @stop