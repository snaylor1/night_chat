<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
//default route
Route::get('/', function()
{
	$posts = get_posts();
  return View::make('pages.home')->withPosts($posts);
});
//default route for home, retrieves post data
Route::get('home', function()
{
  $posts = get_posts();
  return View::make('pages.home')->withPosts($posts);
});
//default route for all comments, gets id
Route::get('all_comments/{id}', function($id)
{
  $posts = get_posts($id);
  return View::make('pages.all_comments')->withPosts($posts);
});
/* Displays post with the given id. */
Route::get('comment_detail/{id}', function ($id)
{
  $post = get_post($id);
	return View::make('pages.comment_detail')->withPost($post);
}); 
//default route for add comments
Route::get('add_comment', function()
{
  return View::make('pages.add_comment');
});
//default route for update comment, gets id
Route::get('update_comment/{id}', function($id)
{
  $post = get_post($id);
  return View::make('pages.update_comment')->withPost($post);
});
//default route for delete comment, gets id for deletion
Route::get('delete_comment/{id}', function($id)
{
  delete_comment($id);

  $posts = get_posts();
  return View::make('pages.home')->withPosts($posts);
});
//add comment action takes user input to use for post table, sets variables to correct user input, then stores into id variable, calls sql add statement
Route::post('add_comment_action', function()
{
  $username = Input::get('username');
  $title = Input::get('title');
  $message = Input::get('message');

  $id = add_comment($username, $title, $message);

  // If successfully created then display newly created post, 
  if ($id) 
  {
    return Redirect::to(url("comment_detail/$id"));
  } 
  else
  {
    die("Error adding comment");
  }
});
//update comment action takes user input to edit post table, sets to new input of users choice, calls update sql statement
Route::post('update_comment_action', function()
{
  $username = Input::get('username');
  $title = Input::get('title');
  $message = Input::get('message');
  $id = Input::get('id');
  
  $id = update_comment($username, $title, $message);
  
  if ($username)
    {
    return Redirect::to(url("home"));
    }
  else
  {
    die("Error adding comment");
  }
  
});
//delete comment action calls sql delete command using id for deletion selection
Route::get('delete_comment_action', function($id)
{
  $id = Input::get('id');
  
  $id = delete_comment($id);
  
  return Redirect::to(url("home"));
});
//gets all posts from posts table and stores them and returns them to webpages
function get_posts()
{
  $sql = "select * from posts";
  $posts = DB::select($sql);
  return $posts;
}
//select specific post with id of user choice
/* Gets post with the given id */
function get_post($id)
{
	$sql = "select id, username, title, message from posts where id = ?";
	$posts = DB::select($sql, array($id));

	// If we get more than one post or no posts, display an error
	if (count($posts) != 1) 
	{
    die("Invalid query or result: $query\n");
  }

  $post = $posts[0];
	return $post;
}
//add comment sql statement
function add_comment($username, $title, $message)
{
  $sql = "insert into posts (username, title, message) values (?, ?, ?)";

  DB::insert($sql, array($username, $title, $message));

  $id = DB::getPdo()->lastInsertId();

  return $id;
}
//update comment sql statement
function update_comment($username, $title, $message)
{
  $sql = "update posts set username = ?, title = ?, message = ? where id = ?";
  
  DB::update($sql, array($username, $title, $message));
}
//delete comment sql statement
function delete_comment($id)
{
  $sql = "delete from posts where id = ?";
  
  DB::delete($sql, array($id));
}
