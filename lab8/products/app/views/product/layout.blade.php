<html>
  <head>
    <title>
        
        @yield('title')
        
    </title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

    @if (Auth::check())
      {{ Session::forget('login_error') }}
      {{ Auth::user()->username }}
      {{ link_to_route('user.logout', 'Logout') }}
    @else
      {{ link_to_route('user.create', 'Create an Account') }}
      </br></br>
      @if (Session::has('login_error'))
      Login failed!
      @endif
      {{ Form::open(array('url'=> secure_url('user/login'))) }}
      {{ Form::label('username', 'Username: ') }}
      {{ Form::text('username') }}
      {{ $errors->first('username') }}
      {{ Form::label('password', 'Password: ') }}
      {{ Form::text('password') }}
      {{ $errors->first('password') }}
      {{ Form::submit('Login') }}
      {{ Form::close() }}
    @endif

  </head>
  
  <body>
      
    @yield('content')
    
  </body>
  
</html>