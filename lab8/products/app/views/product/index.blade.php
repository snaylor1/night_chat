@extends('product.layout')

@section('title')
Products
@stop

@section('content')

<h1>Products</h1>

<ul>
@foreach($products as $product)
<li>{{ link_to_route('product.show', $product->name, array($product->id)) }} ${{{ $product->price }}}</li>
@endforeach
</ul>
@stop
