<?php

class Comment extends Eloquent
{
  function comments()
    {
        return $this->belongsTo('Post');
    }
    
    public static $rules = array(
        'comment' => 'required'
        );
}