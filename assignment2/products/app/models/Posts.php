<?php

class Post extends Eloquent
{
    function posts()
    {
        return $this->hasMany('Comment');
    }
    
    function users()
    {
        return $this->belongsTo('User');
    }
    
    public static $rules = array(
        'title' => 'required',
        'message' => 'required'
        );
}