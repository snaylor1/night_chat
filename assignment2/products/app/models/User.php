<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class User extends Eloquent implements UserInterface, RemindableInterface, StaplerableInterface {

	use UserTrait, RemindableTrait;
	use EloquentTrait;

    public static $rules = array
    (
        'email' => 'required|unique:users'
    );
    
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	 
	public function __construct(array $attributes = array())
	{
	    $this->hasAttachedFile('image', [
	        'styles' => [
	            'medium' => '100x100',
	            'thumb' => '50x50'
	            ]
	        ]);
	        
	        parent::__construct($attributes);
	}
	 
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
        
    public function users()
    {
    	return $this->hasMany('Post');
    }
    
    public function getFullName()
    {
        return $this->fullName;
    }
 
    public function friends()
    {
        return $this->belongsToMany('User', 'friends_users', 'user_id', 'friend_id');
    }
 
    public function addFriend(User $user)
    {
        $this->friends()->attach($user->id);
    }
 
    public function removeFriend(User $user)
    {
        $this->friends()->detach($user->id);
    }
}
