<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('as' => 'home', 'uses' => 'PostController@index'));

//

Route::post('user/login', array('as' => 'user.login', 'uses' => 'UserController@login'));
Route::get('user/logout', array('as' => 'user.logout', 'uses' => 'UserController@logout'));
Route::get('comment/create/{id}', array('as' => 'goat', 'uses' => 'CommentController@create'));
Route::post('user/show', array('as' => 'goat2', 'uses' => 'UserController@show'));

Route::get('post/getAddFriend/{id}', array('as' => 'post.getAddFriend', 'uses' => 'PostController@getAddFriend'));
Route::get('post/getRemoveFriend/{id}', array('as' => 'post.getRemoveFriend', 'uses' => 'PostController@getRemoveFriend'));

//

Route::resource('post', 'PostController');
Route::resource('comment', 'CommentController');
Route::resource('user', 'UserController');
