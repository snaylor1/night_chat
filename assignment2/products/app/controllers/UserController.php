<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::all(); //get all users
		return View::make('user.index', compact('users')); //return with spec. view
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('user.create'); //ALL FUNCTIONS DEFINED EARLIER
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
		$input = Input::all(); //ALL FUNCTIONS DEFINED EARLIER
		
		$v = Validator::make($input, User::$rules);
		
		if ($v->passes())
		{
			$password = $input['password'];
			$encrypted = Hash::make($password);
			$user = new User;
			$user->email = $input['email'];
			$user->password = $encrypted;
			$user->fullName = $input['fullName'];
			$user->dateOfBirth = $input['dateOfBirth'];
			$user->image = $input['image'];
			$user->save();
			return Redirect::route('post.index');
		}
		else
		{
			return Redirect::route('user.create')->withErrors($v);
		}
	}
	
	public function login()
	{
		$userdata = array( //prepare userdata array
			'email' => Input::get('email'), //store email and
			'password' => Input::get('password') //password inputs in userdata array
		);
		
		if (Auth::attempt($userdata)) //if auth attempt is success
		{
		return Redirect::to(URL::previous()); //redirect to previous view
		} 
		else //if login fails
		{
	 	Session::put('login_error', 'login error'); //issue login error session
		return Redirect::to(URL::previous())->withInput(); //return to previous ciew with error
		}
	}

	public function logout()
	{
		Auth::logout(); //log out user
		return Redirect::to('post'); //return home
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{
		$input = Input::all(); //SEARCH FOR USERS FUNCTION
		$email = $input['email'];
		$users = User::where('email',  '=',  $email)->get(); //get users where email field matches email in table
		return View::make('user.show')->withUsers($users); //make view and display with users
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) //ALL FUNCTIONS DEFINED EARLIER
	{
		if(!Auth::check())return Redirect::route('post.index');
		
		$user = User::find($id);
		return View::make('user.edit', compact('user'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) //ALL FUNCTIONS DEFINED EARLIER
	{
		if(!Auth::check())return Redirect::route('product.index'); //if not logged in cannot update, else
		
		$user = User::find($id); //ALL FUNCTIONS DEFINED EARLIER
		
		$input = Input::all();
		
		$v = Validator::make($input, User::$rules);
		
		if ($v->passes())
		{
			$password = $input['password']; //take password field store in password variable
			$encrypted = Hash::make($password); //encrypt user password
			$user->email = $input['email'];
			$user->password = $encrypted; //fill password as encrypted string in database
			$user->fullName = $input['fullName'];
			$user->dateOfBirth = $input['dateOfBirth'];
			$user->image = $input['image'];
			$user->save();
			return Redirect::route('post.index', $user->id);
		}
		else
		{
			return Redirect::route('user.edit', $user->id)->withErrors($v);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
