<?php

class PostController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
		
		if(Auth::check()) //if logged in
		{
			$not_friends = User::where('id', '!=', Auth::user()->id); //where id does not match user logged in id store not friends variable
    		if (Auth::user()->friends->count()) //if logged in user has any number of friends
    		{
      			$not_friends->whereNotIn('id', Auth::user()->friends->modelKeys()); //where friends do not add to the query
    		}
    		$not_friends = $not_friends->get(); // get not friends to logged in user
    	
    	//
    	
			$posts = Post::all(); //get all posts
			$posts = $posts->sortBy('desc'); //sort in descending order
			$posts = Post::paginate(7); //paginate to 7 results
			foreach($posts as $post) //for each post in posts
			{
				$user_id = $post->user_id; //store post user id into user id variable
				$user = User::find($user_id); //find user id store in user
				return View::make('product.index')->withPosts($posts)->withUser($user)->with('not_friends', $not_friends); //make spec. view with posts, users and not friends
			}
		}
		else
		{
			$posts = Post::all(); //all described above
			$posts = $posts->sortBy('desc');
			$posts = Post::paginate(7);
			foreach($posts as $post)
			{
				$user_id = $post->user_id;
				$user = User::find($user_id);
				return View::make('product.index')->withPosts($posts)->withUser($user);
			}
			
		}
		
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		if(!Auth::check())return Redirect::route('post.index'); //if not logged in redirect to post index
		return View::make('product.create'); //else return view to post.create
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all(); //get all inputs
		
		$v = Validator::make($input, Post::$rules); //store post rules in validator
		
		if ($v->passes()) //if rules are adhered to
		{
			$post = new Post(); //make new post
			$post->username = Auth::user()->email; //set post username as auth user email
			$post->title = $input['title']; // fill title with title field
			$post->message = $input['message']; //as above
			$post->type = $input['type'];
			$post->user_id = Auth::user()->id; //as above
			$post->save(); //save post
		
			return Redirect::action('PostController@show', array($post->id)); //redirect route with controller action, pass array with post id
		}
		else
		{
			return Redirect::action('PostController@create')->withErrors($v); //redirec route with controller action with errors from validator
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$post = Post::find($id); //find post if store in post variable
		$comments = Comment::where('post_id',  '=',  $id)->get(); //take comments where comment->post id is equal to $id
		$comments = Comment::paginate(7); //paginate comments to 7
		return View::make('product.show')->withPost($post)->withComments($comments); //return view with post and comments
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if(!Auth::check())return Redirect::back(); //if not logged in redirect back
		$post = Post::find($id); //defined elsewhere
		return View::make('product.edit', compact('post'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$post = Post::find($id); //ALL FUNCTIONS DEFINED EARLIER
		
		$input = Input::all();
		
		$v = Validator::make($input, Post::$rules);
		
		if ($v->passes())
		{
			$post->title = $input['title'];
			$post->message = $input['message'];
			$post->type = $input['type'];
			$post->save();
		
			return Redirect::route('post.index', $post->id);
		}
		else
		{
			return Redirect::action('post.edit', $post->id)->withErrors($v);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$post = Post::find($id); //find post id store in post variable
		$post->delete(); //delete post
		return Redirect::route('post.index'); //redirect to index
	}
	
	public function getAddFriend($id)
	{
		$user = User::find($id); //find user id
  		Auth::user()->addFriend($user); //add friend to auth user using method defined in user model
  		return Redirect::route('post.index'); //redirect back to post
	}
	
	public function getRemoveFriend($id)
	{
		$user = User::find($id); //FUNCTION DEFINED EARLIER
  		Auth::user()->removeFriend($user); //remove friend of auth user using method defined in user model 
  		return Redirect::route('post.index');
	}


}
