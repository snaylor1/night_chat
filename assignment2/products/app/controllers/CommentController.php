<?php

class commentcontroller extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//not used
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($id)
	{
		if(!Auth::check())return Redirect::route('post.index'); //check auth
		$post = Post::find($id); //find post id store in post variable
		return View::make('comment.create', compact('post')); //make view with specified path with post array
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all(); //get all user input
		
		$v = Validator::make($input, Comment::$rules); //validate with comment rules
		
		if($v->passes()) //if rules are adhered to
		{
			$comment = new Comment(); //make new comment
			$comment->post_id = $input['post_id']; //fill post id with post id field
			$comment->comment_username = Auth::user()->email; //fill comment_username with user email entry
			$comment->comment = $input['comment']; //fill comment id with comment field
			$comment->save(); //save comment
			return Redirect::route('post.index'); //redirect to post index
		}
		else
		{
			return Redirect::back()->withErrors($v); //send user back with errors
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$comment = Comment::find($id); //find comment id store in comment variable
		return View::make('comment.show')->withComment($comment); //make view with spec. path with comment variable
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$comment = Comment::find($id); //find comment id and store in comment
		$comment->delete(); //delete comment
		return Redirect::route('post.index'); //route back to post.index
	}


}
