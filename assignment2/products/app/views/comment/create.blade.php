@extends ('product.layout')

@section ('title')
Create Comment
@stop

@section ('content')
{{ Form::open(array('url' => 'comment')) }}
<div class="form-group">
{{ Form::label('comment', 'Comment: ') }}
{{ Form::text('comment') }}
{{ $errors->first('comment') }}
{{ Form::hidden ('post_id', $post->id)}}
</div>
<div class="form-group">
{{ Form::submit('Comment', array('class' => 'btn btn-success')) }}
</div>
{{ Form::close() }}
@stop