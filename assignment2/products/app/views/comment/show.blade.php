@extends('product.layout')

@section('title')
Comment (single)
@stop

@section('content')

<h1>Comment</h1>

<p>Comment: {{{ $comment->comment }}}</p>

{{ Form::open(array('method' => 'DELETE', 'route' => array('comment.destroy', $comment->id))) }}
{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
{{ Form::close() }}

@stop