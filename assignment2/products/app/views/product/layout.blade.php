<html>
  <head>
    <title>
        
        @yield('title')
        
    </title>

     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"> 

     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
     
     {{ HTML::style( asset('css/main.css') ) }}

    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <h2>Night Chat</h2>
          
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li class="active"><a href="#">Create Post</a></li>
                <li><a href="#">Edit Account</a></li>
                <li><a href="#">Search Users</a></li>
              </ul>
            </div>
        </div>
      </div>
    </nav>
    
    <div id="content" class="col-md-4">

    @if (Auth::check())
      {{ Session::forget('login_error') }}
      <img src="{{asset(Auth::user()->image->url('medium')) }}">
      <h6>{{ Auth::user()->email }} is logged in.</h6> </br>
      <h6>{{ Auth::user()->fullName }}</h6> </br>
      {{ link_to_route('user.logout', 'Logout') }}
      </br></br>
    @else
      {{ link_to_route('user.create', 'Create an Account') }}
      </br></br>
        @if (Session::has('login_error'))
          Login failed!
        @endif
      {{ Form::open(array('url'=> secure_url('user/login'))) }}
      <div class="form-group">
      {{ Form::label('email', 'Email: ') }}
      {{ Form::text('email') }}
      {{ $errors->first('email') }}
      </div>
      <div class="form-group">
      {{ Form::label('password', 'Password: ') }}
      {{ Form::text('password') }}
      {{ $errors->first('password') }}
      </div>
      <div class="form-group">
      {{ Form::submit('Login') }}
      </div>
      {{ Form::close() }}
    @endif
    
    
    
    @yield('head')
    
    </div>
    
  </head>
  
  <body>
    
    <div id="content" class="col-md-5">
      
            @yield('content')
            
    </div>
    
  </body>
  
  <footer>
    
    <div id="content" class="col-md-3">
      
            @yield('bottom')
    
  </footer>
  
</html>