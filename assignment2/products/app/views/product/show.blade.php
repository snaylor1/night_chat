@extends('product.layout')

@section('title')
Post (single)
@stop

@section('content')

<h1>Post</h1>

<p>Title: {{{ $post->title }}}</p>
<p>Message: {{{ $post->message }}}</p>

@foreach($comments as $comment)

@if (!Auth::check())

@if ($comment->post_id == $post->id)

<p>Comments: {{ $comment->comment }}</p>

@endif {{-- id against id --}}

@else {{-- auth --}}

@if ($comment->post_id == $post->id)

<p>Comments: {{ $comment->comment }}

@if ($comment->comment_username == Auth::user()->email)

{{ link_to_route('comment.show', 'Delete comment', array($comment->id)) }}</p>

@endif {{-- id against id --}}

@endif {{-- user against email --}}

@endif {{-- auth --}}

@endforeach

{{ $comments->links() }}

<p>{{ link_to_route('post.edit', 'Edit', array($post->id)) }}</p>

@if (Auth::check())

@if ($post->user_id == Auth::user()->id)

{{ Form::open(array('method' => 'DELETE', 'route' => array('post.destroy', $post->id))) }}
{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
{{ Form::close() }}

@endif {{-- --}}

@else

@endif

@stop

