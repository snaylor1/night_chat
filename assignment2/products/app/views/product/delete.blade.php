@extends ('product.layout')

@section ('title')
Delete Post
@stop

@section ('content')
{{ Form::open(array('method' => 'DELETE', 'route' => array('post.destroy', $product->id))) }}
{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
{{ Form::close() }}
@stop