@extends('product.layout')

@section('title')

Posts

@stop

@section('head')

<h2>{{ link_to_route('post.create', 'Create a new post') }}</h2>
<h2>{{ link_to_route('user.index', 'Search for Users') }}</h2>

@if (Auth::check())
<h2>{{ link_to_route('user.edit', 'Edit user account information', array($user->id)) }}</h2>
@endif

<h1 class="subheader">Friends</h1>
  <table style="width:100%;">
    <thead>
      <tr>
        <th>Full Name: </th>
        <th>Email: </th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @if (Auth::check())
      @foreach (Auth::user()->friends as $friend)
      <tr>
          <td>{{ $friend->fullName }}</td>
          <td>{{ $friend->email }}</td>
          <td>{{ link_to_action('PostController@getRemoveFriend', 'Remove friend', array('id' => $friend->id)) }}</td>
      </tr>
      @endforeach
      @else
      @endif
    </tbody>
  </table>

@stop

@section('content')

<h1>Posts</h1>

<ul class ="nav navbar-center">
    
{{-- <img src="{{ asset($user->image->url('thumb')) }}"> --}}
    
@foreach($posts as $post)

@if (!Auth::check())

@if ($post->type == 'public')

<li><h4>{{{ $post->username }}}</h4>{{ link_to_route('post.show', $post->title, array($post->id)) }} {{{ $post->message }}}... </br> This post is {{{ $post->type }}} </br></br> </li>
<li>Please log in to comment...</li></br>

@endif {{-- public --}}

@else {{-- logged in --}}

@if ($post->type == 'private' || 'public' || 'friends')

<li><h4>{{{ $post->username }}}</h4>{{ link_to_route('post.show', $post->title, array($post->id)) }} {{{ $post->message }}}... </br> This post is {{{ $post->type }}} </br></br> </li>
<li>{{ link_to_route('goat', 'Comment', array($post->id, $user->id)) }}</li>

@endif {{-- public, private --}}

@endif {{-- auth --}}

@endforeach

{{ $posts->links() }}

</ul>

@stop

@section('bottom')

<h2 class="subheader">Other People</h2>
  <table style="width:100%;">
    <thead>
      <tr>
        <th>Full name: </th>
        <th>Email: </th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @if (Auth::check())
      @foreach ($not_friends as $friend)
      <tr>
          <td>{{ $friend->getFullName() }}</td>
          <td>{{ $friend->email }}</td>
          <td>{{ link_to_action('PostController@getAddFriend', 'Add friend', array('id' => $friend->id)) }}</td>
      </tr>
      @endforeach
      @else
      @endif
    </tbody>
  </table>

@stop