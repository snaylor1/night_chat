@extends ('product.layout')

@section ('title')
Edit Post
@stop

@section ('content')

<h1>Update Post</h1>

{{ Form::model($post, array('method' => 'PUT', 'route' => array('post.update', $post->id))); }}
<div class="form-group">
{{ Form::label('title', 'Title: ') }}
{{ Form::text('title') }}
{{ $errors->first('title') }}
</div>
<div class="form-group">
{{ Form::label('message', 'Message: ') }}
{{ Form::text('message') }}
{{ $errors->first('message') }}
</div>
<div class="form-group">
{{ Form::label('Public') }}
{{ Form::radio('type', 'public', true) }}</br>
{{ Form::label('Private') }}
{{ Form::radio('type', 'private') }}</br>
{{ Form::label('Friends') }}
{{ Form::radio('type', 'friends') }}</br>
</div>
<div class="form-group">
{{ Form::submit('Edit', array('class' => 'btn btn-info')) }}
</div>
{{ Form::close() }}

@stop