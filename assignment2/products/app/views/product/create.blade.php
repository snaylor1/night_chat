@extends ('product.layout')

@section ('title')
Create Post
@stop

@section ('content')
{{ Form::open(array('action' => 'PostController@store')) }}
<div class="form-group">
{{ Form::label('title', 'Title: ') }}
{{ Form::text('title') }}
{{ $errors->first('title') }}
</div>
<div class="form-group">
{{ Form::label('message', 'Message: ') }}
{{ Form::text('message') }}
{{ $errors->first('message') }}
</div>
<div class="form-group">
{{ Form::label('Public') }}
{{ Form::radio('type', 'public', true) }}</br>
{{ Form::label('Private') }}
{{ Form::radio('type', 'private') }}</br>
{{ Form::label('Friends') }}
{{ Form::radio('type', 'friends') }}</br>
</div>
<div class="form-group">
{{ Form::submit('Create', array('class' => 'btn btn-success')) }}
</div>
{{ Form::close() }}
@stop