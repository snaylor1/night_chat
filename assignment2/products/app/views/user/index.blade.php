@extends('product.layout')

@section('title')
Search users
@stop

@section('content')
<h1>Search Users</h1>
{{ Form::open(array('method' => 'POST', 'action' => 'UserController@show')) }}
{{ Form::label('email', 'E-Mail Address: ') }}
{{ Form::text('email') }}
{{ Form::submit('Search', array('class' => 'btn btn-warning')) }}
{{ Form::close() }}
@stop