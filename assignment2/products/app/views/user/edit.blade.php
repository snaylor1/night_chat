@extends ('product.layout')

@section ('title')
Edit User
@stop

@section ('content')

<h1>Update User</h1>

{{ Form::model($user, array('route' => array('user.update', $user->id), 'files' => true, 'method' => 'PUT')) }}
<div class="form-group">
{{ Form::label('email', 'Email: ') }}
{{ Form::text('email') }}
{{ $errors->first('email') }}
</div>
<div class="form-group">
{{ Form::label('password', 'Password: ') }}
{{ Form::text('password') }}
{{ $errors->first('password') }}
</div>
<div class="form-group">
{{ Form::label('fullName', 'Full Name: ') }}
{{ Form::text('fullName') }}
</div>
<div class="form-group">
{{ Form::label('dateOfBirth', 'D.O.B: ') }}
{{ Form::text('dateOfBirth') }}
</div>
<div class="form-group">
{{ Form::label('image', 'Image: ') }}
{{ Form::file('image') }}
</div>
<div class="form-group">
{{ Form::submit('Edit', array('class' => 'btn btn-info')) }}
</div>
{{ Form::close() }}
@stop