@extends('product.layout')

@section('title')
Posts
@stop

@section('content')

{{ Form::open(array('action' => 'UserController@store', 'files' => true)) }}
<div class="form-group">
{{ Form::label('email', 'Email: ') }}
{{ Form::text('email') }}
{{ $errors->first('email') }}
<div/>
<div class="form-group">
{{ Form::label('password', 'Password: ') }}
{{ Form::text('password') }}
<div/>
<div class="form-group">
{{ Form::label('fullName', 'Full Name: ') }}
{{ Form::text('fullName') }}
<div/>
<div class="form-group">
{{ Form::label('dateOfBirth', 'D.O.B: ') }}
{{ Form::text('dateOfBirth') }}
<div/>
<div class="form-group">
{{ Form::label('image', 'Image: ') }}
{{ Form::file('image') }}
<div/>
<div class="form-group">
{{ Form::submit('Create', array('class' => 'btn btn-success')) }}
<div/>
{{ Form::close() }}

@stop
