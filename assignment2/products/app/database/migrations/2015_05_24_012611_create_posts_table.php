<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('username');
			$table->string('title');
			$table->string('message');
			$table->string('type');
			$table->integer('user_id')->references('id')->on('users');
			$table->string('remember_token')->nullable();
			$table->timestamps();
		});
		
		Schema::create('comments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('post_id')->references('id')->on('posts');
			$table->string('comment_username')->references('email')->on('users');
			$table->string('comment');
			$table->timestamps();
		});
		
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('email')->unique();
			$table->string('password')->index();
			$table->string('fullName');
			$table->String('dateOfBirth');
			$table->string('remember_token')->nullable();
			$table->timestamps();
		});
		
		Schema::create('friends_users', function(Blueprint $table)
    	{
      		$table->integer('friend_id')->unsigned();
      		$table->integer('user_id')->unsigned();
 
      		$table->foreign('user_id')->references('id')->on('users');
      		$table->foreign('friend_id')->references('id')->on('users');
 
      		$table->primary(array('user_id', 'friend_id'));
    	});
		
		/*
		Schema::table('users', function(Blueprint $table) { 
            
            $table->string('image_file_name')->nullable();
            $table->integer('image_file_size')->nullable();
            $table->string('image_content_type')->nullable();
            $table->timestamp('image_updated_at')->nullable();

        });
		*/
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('posts');
		
		Schema::dropIfExists('comments');
		
		Schema::dropIfExists('users');
		
		Schema::dropIfExists('friends_users');
		/*
		Schema::table('users', function(Blueprint $table) {

            $table->dropColumn(array('image_file_name', 'image_file_size', 'image_content_type', 'image_updated_at'));
            
        });
		*/
	}

}
