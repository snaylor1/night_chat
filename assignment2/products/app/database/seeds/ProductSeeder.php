<?php

class ProductSeeder extends Seeder {
    public function run()
    {
    $product = new Product;
    $product->name = 'Coke';
    $product->price = 2.00;
    $product->save();
    
    $product = new Product;
    $product->name = 'Hot Dogs';
    $product->price = 4.00;
    $product->save();
    }
}
