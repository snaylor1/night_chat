<?php

class PostSeeder extends Seeder {
    public function run()
    {
    $post = new Post;
    $post->username = 'Mark';
    $post->title = 'Click bait';
    $post->message = 'This is post 1 for rubric';
    $post->type = 'public';
    $post->user_id = 1;
    $post->save();
    
    $post = new Post;
    $post->username = 'Michael';
    $post->title = 'Look at my title';
    $post->message = 'This is post 2 for rubric';
    $post->type = 'public';
    $post->user_id = 1;
    $post->save();
    
    $post = new Post;
    $post->username = 'Joe';
    $post->title = 'Look at my title';
    $post->message = 'This is post 3 for rubric';
    $post->type = 'public';
    $post->user_id = 1 ;
    $post->save();
    
    $post = new Post;
    $post->username = 'Jim';
    $post->title = 'Look at my title';
    $post->message = 'This is post 4 for rubric';
    $post->type = 'public';
    $post->user_id = 1;
    $post->save();
    
    $post = new Post;
    $post->username = 'Michael';
    $post->title = 'Look at my title';
    $post->message = 'This is post 5 for rubric';
    $post->type = 'public';
    $post->user_id = 1;
    $post->save();
    
    $post = new Post;
    $post->username = 'Michael';
    $post->title = 'Look at my title';
    $post->message = 'This is post 6 for rubric';
    $post->type = 'public';
    $post->user_id = 1;
    $post->save();
    
    $post = new Post;
    $post->username = 'Michael';
    $post->title = 'Look at my title';
    $post->message = 'This is post 7 for rubric';
    $post->type = 'public';
    $post->user_id = 1;
    $post->save();
    
    $post = new Post;
    $post->username = 'Michael';
    $post->title = 'Look at my title';
    $post->message = 'This is post 8 for rubric';
    $post->type = 'public';
    $post->user_id = 1;
    $post->save();
    
    $post = new Post;
    $post->username = 'Michael';
    $post->title = 'Look at my title';
    $post->message = 'This is post 9 for rubric';
    $post->type = 'public';
    $post->user_id = 1;
    $post->save();
    
    $post = new Post;
    $post->username = 'Michael';
    $post->title = 'Look at my title';
    $post->message = 'This is post 10 for rubric';
    $post->type = 'public';
    $post->user_id = 1;
    $post->save();
    
    $post = new Post;
    $post->username = 'Michael';
    $post->title = 'Look at my title';
    $post->message = 'This is post 10 for rubric';
    $post->type = 'private';
    $post->user_id = 1;
    $post->save();
    }
}