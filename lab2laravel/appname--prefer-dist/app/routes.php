<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
require "models/arrays.php";

Route::get('/', function ()
{
    $posts = array("27th Jan 2014 - Hiya", "20th Feb 2015 - Hello", "13th Mar 2016 - Heya");
    return View::make('posts', array('posts' => $posts));
});

Route::get('friends', function()
{
    $friends = array("Tim", "Mike", "Colin");
    return View::make('friends', array('friends' => $friends));
});

Route::get('posts', function()
{
    $posts = array("27th Jan 2014 - Hiya", "20th Feb 2015 - Hello", "13th Mar 2016 - Heya");
    return View::make('posts', array('posts' => $posts));
});