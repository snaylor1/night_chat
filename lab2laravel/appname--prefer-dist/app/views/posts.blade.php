<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body>
<div class="container">

    <header class="row">
        @include('includes.header')
    </header>

    <div id="main" class="row">

            @yield('content')
            
            <div class="container">
                    <div class = "row">
                        <div class="col-md-3" id="left">
                            <h3>Posts:</h3><br><br>
                        </div>
                        <div class="col-md-6">
                            <br>
                            @forelse ($posts as $post)
                            <p>The post says: {{$post}}.</p>
                            @empty
                            <p>No posts made by people.</p>
                            @endforelse <br><br>
                        </div>
                        <div class="col-md-3" id="right">
                            {{-- Right --}}<br><br>
                        </div>
                    </div>
                </div><!-- /.container -->
                
    </div>

    <footer class="row">
        @include('includes.footer')
    </footer>

</div>
</body>
</html>